import metodos.Matematicas2;

public class Matematiicas {

	public static void main(String[] args) {
		System.out.println(Matematicas2.calcularFactorial(4));
		System.out.println();
		System.out.println(Matematicas2.restar(5, 2));
		System.out.println();
		System.out.println(Matematicas2.media(5, 4, 9));
		System.out.println();
		System.out.println(Matematicas2.dividir(5, 2));
		System.out.println();
		System.out.println(Matematicas2.cubo(4));
	}

}
