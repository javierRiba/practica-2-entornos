package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JDesktopPane;
import java.awt.Button;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JToolBar;
import javax.swing.JButton;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JCheckBox;
import javax.swing.ButtonGroup;
import java.awt.Color;
import java.awt.Toolkit;
import javax.swing.JScrollPane;
import javax.swing.border.EtchedBorder;
import javax.swing.ScrollPaneConstants;
import java.awt.TextArea;
import java.awt.Scrollbar;
import javax.swing.JSlider;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JSpinner;
import javax.swing.SpinnerListModel;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Canvas;
import java.awt.ScrollPane;
import javax.swing.JEditorPane;
import javax.swing.ImageIcon;
/**
 * 
 * @author javi
 * @since 17/01/2018
 * 
 */



public class ventanaa extends JFrame {

	private JPanel contentPane;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();
	private JTextField textField_1;
	private JTextField textField;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private final ButtonGroup buttonGroup_2 = new ButtonGroup();
	private final ButtonGroup buttonGroup_3 = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ventanaa frame = new ventanaa();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ventanaa() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(ventanaa.class.getResource("/imagenes/motoss.jpg")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 936, 688);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnArchivo = new JMenu("archivo");
		menuBar.add(mnArchivo);
		
		JMenuItem mntmAbrir = new JMenuItem("ABRIR");
		mnArchivo.add(mntmAbrir);
		
		JMenuItem mntmGuardar = new JMenuItem("GUARDAR");
		mnArchivo.add(mntmGuardar);
		
		JMenuItem mntmCerrar = new JMenuItem("CERRAR");
		mnArchivo.add(mntmCerrar);
		
		JMenu mnEditar = new JMenu("editar");
		menuBar.add(mnEditar);
		
		JMenuItem mntmCambiarLetra = new JMenuItem("CAMBIAR LETRA");
		mnEditar.add(mntmCambiarLetra);
		
		JMenuItem mntmCambiarFondo = new JMenuItem("CAMBIAR FONDO");
		mnEditar.add(mntmCambiarFondo);
		
		JMenuItem mntmCambiarColor = new JMenuItem("CAMBIAR COLOR");
		mnEditar.add(mntmCambiarColor);
		
		JMenu mnNavegar = new JMenu("navegar");
		menuBar.add(mnNavegar);
		
		JMenuItem mntmGooglectrlf = new JMenuItem("google (ctrl+f5)");
		mnNavegar.add(mntmGooglectrlf);
		
		JMenuItem mntmMozillactrlf = new JMenuItem("mozilla (ctrl+f7)");
		mnNavegar.add(mntmMozillactrlf);
		
		JMenuItem mntmExplorerctrlf = new JMenuItem("explorer (ctrl+f9)");
		mnNavegar.add(mntmExplorerctrlf);
		
		JMenu mnEjecutar = new JMenu("ejecutar");
		menuBar.add(mnEjecutar);
		
		JMenuItem mntmEjecutarctrl = new JMenuItem(" ejecutar (ctrl + F12)");
		mnEjecutar.add(mntmEjecutarctrl);
		
		JMenu mnAyuda = new JMenu("ayuda");
		menuBar.add(mnAyuda);
		
		JMenuItem mntmWindowsHepl = new JMenuItem("WINDOWS HELP");
		mnAyuda.add(mntmWindowsHepl);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JToolBar toolBar = new JToolBar();
		contentPane.add(toolBar, BorderLayout.NORTH);
		
		JButton btnBuscar = new JButton("Buscar");
		toolBar.add(btnBuscar);
		
		JButton btnProjecto = new JButton("Guardar");
		toolBar.add(btnProjecto);
		
		JButton btnInsertar = new JButton("insertar");
		toolBar.add(btnInsertar);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(tabbedPane, BorderLayout.CENTER);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.LIGHT_GRAY);
		panel_1.setForeground(Color.RED);
		tabbedPane.addTab("Motos", null, panel_1, null);
		panel_1.setLayout(null);
		
		JButton btnNewButton = new JButton("Modelo\r\n");
		btnNewButton.setBounds(22, 13, 116, 25);
		panel_1.add(btnNewButton);
		
		JLabel lblCursos = new JLabel("Marcas");
		lblCursos.setBounds(32, 56, 56, 25);
		panel_1.add(lblCursos);
		
		JCheckBox chckbxGuarderia = new JCheckBox("Scooter");
		chckbxGuarderia.setBounds(153, 13, 78, 25);
		panel_1.add(chckbxGuarderia);
		
		JCheckBox chckbxInfantil = new JCheckBox("Enduro");
		chckbxInfantil.setBounds(241, 13, 69, 25);
		panel_1.add(chckbxInfantil);
		
		JCheckBox chckbxPrimaria = new JCheckBox("trail");
		chckbxPrimaria.setBounds(314, 13, 56, 25);
		panel_1.add(chckbxPrimaria);
		
		JCheckBox chckbxSecundaria = new JCheckBox("Carretera\r\n");
		chckbxSecundaria.setBounds(374, 13, 83, 25);
		panel_1.add(chckbxSecundaria);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(638, 434, 2, 2);
		panel_1.add(scrollPane);
		
		JLabel lblPrecio = new JLabel("Precio");
		lblPrecio.setBounds(32, 326, 56, 16);
		panel_1.add(lblPrecio);
		
		JSlider slider = new JSlider();
		slider.setMinimum(1000);
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		slider.setMaximum(30000);
		slider.setMajorTickSpacing(2000);
		slider.setBounds(22, 377, 680, 81);
		panel_1.add(slider);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"KTM", "YAMAHA", "KAWASAKI", "BETA", "RIEJU", "HONDA", "BMW", "INDIAN"}));
		comboBox.setBounds(93, 57, 138, 22);
		panel_1.add(comboBox);
		
		JCheckBox chckbxClasicas = new JCheckBox("Clasicas\r\n");
		chckbxClasicas.setBounds(461, 13, 83, 25);
		panel_1.add(chckbxClasicas);
		
		JLabel lblCilindrada = new JLabel("Cilindrada");
		lblCilindrada.setBounds(32, 123, 69, 25);
		panel_1.add(lblCilindrada);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setModel(new DefaultComboBoxModel(new String[] {"49cc", "125cc", "250cc", "300cc", "500cc", "750cc", "800cc", "1000cc"}));
		comboBox_1.setBounds(124, 124, 103, 22);
		panel_1.add(comboBox_1);
		
		JLabel lblEquipamiento = new JLabel("Equipamiento");
		lblEquipamiento.setBounds(32, 186, 83, 25);
		panel_1.add(lblEquipamiento);
		
		JCheckBox chckbxPortaBultos = new JCheckBox("Porta bultos");
		chckbxPortaBultos.setBounds(119, 186, 103, 25);
		panel_1.add(chckbxPortaBultos);
		
		JCheckBox chckbxManetas = new JCheckBox("Manetas");
		chckbxManetas.setBounds(226, 186, 78, 25);
		panel_1.add(chckbxManetas);
		
		JCheckBox chckbxMonoYGuantes = new JCheckBox("Mono y guantes\r\n");
		chckbxMonoYGuantes.setBounds(314, 186, 126, 25);
		panel_1.add(chckbxMonoYGuantes);
		
		JCheckBox chckbxCasco = new JCheckBox("Casco\r\n");
		chckbxCasco.setBounds(444, 186, 78, 25);
		panel_1.add(chckbxCasco);
		
		JCheckBox chckbxKitCompleto = new JCheckBox("Kit completo");
		chckbxKitCompleto.setBounds(533, 186, 103, 25);
		panel_1.add(chckbxKitCompleto);
		
		JSpinner spinner = new JSpinner();
		spinner.setModel(new SpinnerListModel(new String[] {"rojo", "azul", "verde", "negra", "blanca", "violeta", "personalizar"}));
		spinner.setBounds(119, 255, 103, 25);
		panel_1.add(spinner);
		
		JLabel lblColor = new JLabel("Color");
		lblColor.setBounds(32, 255, 83, 25);
		panel_1.add(lblColor);
		
		JRadioButton rdbtnEnviar = new JRadioButton("Enviar");
		buttonGroup_3.add(rdbtnEnviar);
		rdbtnEnviar.setBounds(46, 501, 127, 25);
		panel_1.add(rdbtnEnviar);
		
		JRadioButton radioButton_2 = new JRadioButton("Eliminar");
		buttonGroup_3.add(radioButton_2);
		radioButton_2.setBounds(208, 501, 126, 25);
		panel_1.add(radioButton_2);
		
		JRadioButton rdbtnGuardarPedido = new JRadioButton("Guardar pedido\r\n");
		buttonGroup_3.add(rdbtnGuardarPedido);
		rdbtnGuardarPedido.setBounds(374, 501, 147, 25);
		panel_1.add(rdbtnGuardarPedido);
		
		JButton btnSiguiente = new JButton("Siguiente\r\n");
		btnSiguiente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnSiguiente.setBounds(638, 501, 190, 25);
		panel_1.add(btnSiguiente);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Usuario", null, panel, null);
		panel.setLayout(null);
		
		JPanel panel_2 = new JPanel();
		panel_2.setLayout(null);
		panel_2.setForeground(Color.RED);
		panel_2.setBackground(new Color(255, 255, 153));
		panel_2.setBounds(0, 0, 903, 550);
		panel.add(panel_2);
		
		JLabel label_3 = new JLabel("Sexo");
		label_3.setBounds(22, 106, 56, 21);
		panel_2.add(label_3);
		
		JCheckBox checkBox = new JCheckBox("Hombre");
		checkBox.setBounds(86, 104, 113, 25);
		panel_2.add(checkBox);
		
		JCheckBox checkBox_1 = new JCheckBox("Mujer");
		checkBox_1.setBounds(212, 106, 113, 25);
		panel_2.add(checkBox_1);
		
		JRadioButton radioButton = new JRadioButton("Insertar");
		buttonGroup_2.add(radioButton);
		radioButton.setBounds(173, 482, 127, 25);
		panel_2.add(radioButton);
		
		JRadioButton radioButton_1 = new JRadioButton("Eliminar");
		buttonGroup_2.add(radioButton_1);
		radioButton_1.setBounds(448, 482, 127, 25);
		panel_2.add(radioButton_1);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(135, 289, 116, 22);
		panel_2.add(textField_1);
		
		JLabel label_6 = new JLabel("Codigo postal");
		label_6.setBounds(22, 292, 101, 16);
		panel_2.add(label_6);
		
		JLabel label_7 = new JLabel("Nombre");
		label_7.setBounds(22, 24, 56, 25);
		panel_2.add(label_7);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(638, 434, 2, 2);
		panel_2.add(scrollPane_1);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(83, 25, 141, 22);
		panel_2.add(textField);
		
		JLabel lblApellidos = new JLabel("Apellidos\r\n");
		lblApellidos.setBounds(22, 62, 56, 25);
		panel_2.add(lblApellidos);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(86, 60, 251, 22);
		panel_2.add(textField_2);
		
		JLabel lblDireccion = new JLabel("Direccion");
		lblDireccion.setBounds(22, 156, 101, 16);
		panel_2.add(lblDireccion);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(97, 153, 363, 22);
		panel_2.add(textField_3);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(97, 188, 116, 22);
		panel_2.add(textField_4);
		
		textField_5 = new JTextField();
		textField_5.setColumns(10);
		textField_5.setBounds(97, 223, 116, 22);
		panel_2.add(textField_5);
		
		JLabel lblPortal = new JLabel("Portal");
		lblPortal.setBounds(22, 191, 101, 16);
		panel_2.add(lblPortal);
		
		JLabel lblPiso = new JLabel("Piso");
		lblPiso.setBounds(22, 226, 101, 16);
		panel_2.add(lblPiso);
		
		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(523, 344, 189, 77);
		panel_2.add(scrollPane_2);
		
		JTextArea textArea = new JTextArea();
		scrollPane_2.setViewportView(textArea);
		
		JLabel lblInformacionAdicional = new JLabel("Informacion adicional");
		lblInformacionAdicional.setBounds(498, 315, 142, 16);
		panel_2.add(lblInformacionAdicional);
		
		JButton btnSiguiente_1 = new JButton("Siguiente");
		btnSiguiente_1.setBounds(726, 482, 116, 25);
		panel_2.add(btnSiguiente_1);
		
		JButton button = new JButton("New button");
		button.setForeground(new Color(255, 255, 153));
		button.setBackground(new Color(255, 255, 153));
		button.setIcon(new ImageIcon(ventanaa.class.getResource("/imagenes/motooo.png")));
		button.setBounds(606, 20, 189, 166);
		panel_2.add(button);
	}
}
